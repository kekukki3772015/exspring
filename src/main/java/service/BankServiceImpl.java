package service;

import java.text.MessageFormat;

import model.Money;

public class BankServiceImpl implements BankService {

    @Override
    public void credit(Money money, String fromAccount) {
        System.out.println(
                MessageFormat.format("credit {0} from account number {1}",
                        money, fromAccount));
    }

    @Override
    public void debit(Money money, String toAccount) {
        System.out.println(
                MessageFormat.format("debit {0} to account number {1}",
                        money, toAccount));
    }

}
