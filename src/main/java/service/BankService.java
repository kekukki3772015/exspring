package service;

import model.Money;

public interface BankService {

    void credit(Money money, String fromAccount);

    void debit(Money money, String toAccount);

}