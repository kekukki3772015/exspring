package service;

import model.Money;

public class TransferService {

    private BankService bankService;

    public void transfer(Money money, String fromAccount, String toAccount) {
        bankService.credit(money, fromAccount);

        bankService.debit(money, toAccount);
    }

    public BankService getBankService() {
        return bankService;
    }

    public void setBankService(BankService bankService) {
        this.bankService = bankService;
    }
}