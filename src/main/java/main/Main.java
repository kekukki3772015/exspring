package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import config.Config;

public class Main {

    public static void main(String[] args) throws Exception {
        ApplicationContext ctx =
              new AnnotationConfigApplicationContext(Config.class);



        ((ConfigurableApplicationContext) ctx).close();
    }
}